#!/bin/bash

# script based on https://gist.github.com/jniltinho/be80254594ae245a9ec8fc7c2f8d4863
# adjusted to work with OpenSuSe Tumbleweed (on 2018-07-04)

# mcrypt officially doesn't exist in Tumbleweed, taken from https://pkgs.org/download/libmcrypt for OpenSuSe Leap
rpm -ivh --force mcrypt/libmcrypt-2.5.8-120.3.x86_64.rpm
rpm -ivh --force mcrypt/libmcrypt-devel-2.5.8-120.3.x86_64.rpm

# install all we need for compiling
zypper in openssl-devel
zypper in gcc gcc-c++ libxml2-devel pkgconfig libbz2-devel curl-devel libwebp-devel
zypper in libpng12-devel libpng16-devel libjpeg62-devel libxmp-devel freetype-devel
zypper in gmp-devel gd-devel freetype2-devel imap-devel
zypper in aspell-devel autoconf bison re2c libicu-devel
zypper in libbz2-devel libedit-devel libevent-devel db-devel gmp-devel krb5-devel
zypper in libicu-devel libjpeg-devel libopenssl-devel libpng-devel
zypper in libtidy-devel libtiff-devel libtool libxslt-devel postgresql-devel
zypper in libXpm-devel

# create folders
mkdir -p /opt/php-71
mkdir /usr/local/src/php71-build

#fetch php 7.1.19 (compatible with Magento 2)
cd /usr/local/src/php71-build
wget http://uk3.php.net/get/php-7.1.19.tar.bz2/from/this/mirror -O php-7.1.19.tar.bz2
tar jxf php-7.1.19.tar.bz2
cd php-7.1.19/


#configure
./configure --prefix=/opt/php-7.1 --with-pdo-pgsql --with-zlib-dir --with-freetype-dir --enable-mbstring \
--with-libxml-dir=/usr --enable-soap --enable-intl --enable-calendar --with-curl --with-mcrypt --with-zlib \
--with-gd --with-pgsql --disable-rpath --enable-inline-optimization --with-bz2 --with-zlib --enable-sockets \
--enable-sysvsem --enable-sysvshm --enable-pcntl --enable-mbregex --enable-exif --enable-bcmath --with-mhash \
--enable-zip --with-pcre-regex --with-pdo-mysql --with-mysqli --with-mysql-sock=/var/run/mysql/mysql.sock \
--with-xpm-dir=/usr --with-webp-dir=/usr --with-jpeg-dir=/usr --with-png-dir=/usr --enable-gd-native-ttf \
--with-openssl --with-fpm-user=wwwrun --with-fpm-group=www --with-libdir=lib64 --enable-ftp --with-imap \
--with-imap-ssl --with-kerberos --with-gettext --with-xmlrpc --with-xsl --enable-opcache --enable-fpm

#compile using 2 cores, want to us more? change the number
make -j 2

#install
make install



#copy configuration files
cp /usr/local/src/php71-build/php-7.1.19/php.ini-production /opt/php-7.1/php.ini
cp /usr/local/src/php71-build/php-7.1.19/sapi/fpm/php-fpm.conf  /opt/php-7.1/etc/php-fpm.conf
cp /usr/local/src/php71-build/php-7.1.19/sapi/fpm/www.conf  /opt/php-7.1/etc/php-fpm.d/www.conf


# do some magic replace
## Mudar ini do PHP
for i in /opt/php-7.*/php.ini;do
sed -i 's|max_execution_time = 30|max_execution_time = 120|' $i
sed -i 's|upload_max_filesize = 2M|upload_max_filesize = 32M|' $i
sed -i 's|post_max_size = 8M|post_max_size = 32M|' $i
sed -i 's|error_reporting = E_ALL & ~E_DEPRECATED|error_reporting =  E_ERROR|' $i
sed -i 's|short_open_tag = Off|short_open_tag = On|' $i
sed -i "s|;date.timezone =|date.timezone = 'America\/Sao_Paulo'|" $i
done

# enable opcache
echo 'zend_extension=opcache.so' >> /opt/php-71/php.ini


# prepare php-fpm
### Change PHP-FPM Config
sed -i "s|;pid = run/php-fpm.pid|pid = run/php-fpm.pid|" /opt/php-7.1/etc/php-fpm.conf
#sed -i "s|listen = 127.0.0.1:9000|listen = 127.0.0.1:8999|" /opt/php-71/etc/php-fpm.d/www.conf
sed -i "s|;include=etc/fpm.d/\*.conf|include=/opt/php-7.1/etc/php-fpm.d/\*.conf|" /opt/php-7.1/etc/php-fpm.conf

echo '[Unit]
Description=The PHP 7.1 FastCGI Process Manager
After=network.target
[Service]
Type=simple
PIDFile=/opt/php-7.1/var/run/php-fpm.pid
ExecStart=/opt/php-7.1/sbin/php-fpm --nodaemonize --fpm-config /opt/php-7.1/etc/php-fpm.conf
ExecReload=/bin/kill -USR2 $MAINPID
[Install]
WantedBy=multi-user.target' > /usr/lib/systemd/system/php-71-fpm.service

# start at boot
systemctl enable php-71-fpm.service
systemctl daemon-reload
systemctl start php-71-fpm.service